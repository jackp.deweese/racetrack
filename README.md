# Racetrack 2: Electric Boogaloo
### A Shiny App to Feed Your Nostalgia

A digital version of the **paper and pencil game Racetrack** taken from its humble beginnings on graph paper in the ‘60s to a fully online click-to-drive game, Racetrack 2: Electric Boogaloo (R2EB)  takes particular advantage of **Shiny’s reactivity across global variables** to create an entirely multiplayer experience with a TRON-inspired theme.

_Jump into the vehicle of your choice and start racing to set records and unlock new vehicles – just be careful of those sharp turns!_

----

## Login
 
R2EB has a **built-from-scratch login system** that tries to alleviate the headache of yet another password to remember by simply associating your account with your email. Choose a username and put in your email and click [SEND CODE] to receive your **one-time authorization code** (OATC). Just copy and paste that into the code field, and then choose one of the three starter vehicles to start racing.

The app will remember you based on an **encrypted cookie**, and if you need to login again, just enter your email for another OATC code, choose from any of your unlocked vehicles, and start your engines.

To aid users through the login process R2EB can notify a user if their email is not recognized as a current user and in the case that a OATC email is lost there is a button available to resend the token.
 
## Directory
 
After successfully logging in, you are taken to the landing page that has a three-panel structure. From left to right the panels are a navigation pane, a register of active games, and finally a directory of online users.  The **navigation panel** presents you with the various features available in the tool which are discussed more in-depth below. The center **register of games** gives meta-data about the active games so you know which races are available to join and which can be spectated. Finally, the right pane provides information about **online players** and their race staus.
 
## New Game

The reason we’re all here – to race! Before continuing, if you’ve never heard of Racetrack before, the Wikipedia page has a succinct description of the original game for better context: [Racetrack Wikipedia Page](https://en.wikipedia.org/wiki/Racetrack_\(game\)#The_basic_game). There’s also an in-app Rules section described later on.

When starting a game, the settings to choose from are the **track** itself with a preview, the **number of players**, the **speed** of turn timer, and how many **laps** the race will be.
 
The game itself has a clean, simple aesthetic to recreate graph paper on the screen with the various vehicle icons for some extra flavor. Each turn a player moves their vehicle by clicking any of the **nine available grid points** (the highlighted square is centered on the previous move transposed to the vehicle’s current position). This type of movement captures some elements of inertia in real racing, requiring quick thinking to strategize the fastest route that doesn’t land the vehicle into a wall.
 
The left panel provides a **turn indicator** to show who’s up and how much time they have left. Below that is a list of players to show lap progress and who’s crashed or finished the race. A pencil and ruler represent a player still racing as a homage to the original game.
 
## Leaderboard
 
Like any classic arcade game, what would R2EB be without a **high scores screen**? There’s an overall leaderboard for the players with the most wins over losses racing, as well as track records for the top ten best times for each track. _Note that “times” are in units of turns, not seconds – the seconds timer is just to stop players from taking too long to move._
 
## Practice
 
There’s also an **untimed, single-player mode** to hone your racing skills and get all of those pesky, embarrassing crashes out of the way. You can practice on any track, and easily restart if you get in a bind, but no records can be set in practice mode.
 
## Design Course
 
To keep the game exciting after you’ve raced the main tracks, there’s a **track designer** that lets everyone contribute their own course ideas to the database. The designer will even do its best to give you warning messages when the track isn’t drive-able. You can **undo points** if you make a mistake (or just start the whole thing over), and the whole grid is **resizable** if you want to make an extra-large track. Once you save the course it will automatically redirect you to the Practice section to try out your very own creation.
 
## Rules
 
The game isn’t overly complicated fortunately, but there are a few **animated diagrams** to better explain the mechanics of the movement and examples of crashes.
 
## User Settings
 
The user settings modal shows all of the 24 vehicles to choose from that you see every time you join the app as well as a logout option.

A new user starts with three vehicle options: a sedan, a motorcycle, and a pickup truck. The remaining 21 vehicles all have a **unique requirement to unlock**, from easy ones like crashing in a race to unlock the shopping cart to the most difficult – unlocking every other vehicle to use the coveted rocket ship. Each unlock title even has a reference to the vehicle, whether real or fictional.
 
## Code Highlights

The core of the **online multiplayer functionality** boils down to Shiny’s ability to have **global variables that persist across all sessions**. (This was tested extensively first with online multiplayer versions of the dice and board games Yahtzee and Parcheesi back in April of 2020 as a way for my family to continue playing board games during quarantine.) Using a reactive value in the global space of Shiny, every move a player makes shows up seamlessly for the competing players making the networking aspect of the game nearly effortless.

While R’s base plotting functions have been largely abandoned for more out-of-the-box aesthetic packages, it lent itself well to a simple code base with the grid-like nature of Racetrack. The showtext package gives the game plot the Font Awesome icons, which contribute heavily to the overall aesthetic of R2EB, with just a little help from stringi’s Unicode “unescape” function to handle dynamically inserting the vehicle icon codes.

The sf package drives all of the **calculations of crashes and crossing the finish line** in an efficient enough manner to not slow down live gameplay. The outer and inner walls of a track are converted to **polygon shape objects** at the beginning of a race and stored alongside the other game state variables in the global reactive values list, so each move can also be converted to a simple line shape to check for intersections.

The home-brew authentication system was built with the safer and blastula R packages and JavaScript Cookie. After blastula sends the **verification code** and the user enters it, safer encrypts their username and it’s left as a cookie for **Shiny to bind as an input** for future sessions. Because shinyapps.io uses HTTPS and the encryption and decryption are both done server-side, an asymmetric set of keys is unnecessary. _Note that the encryption key is changed from the placeholder published to a public git repo before publishing the app._

While the database as the app exists now is just a local file, the app is set up so that a single function can be tweaked to take a **new database connection** to make R2EB more portable. The SQL queries were even written in a moderate effort to be as **universally compatible** as possible.

Finally, packages like tippy, shinyWidgets, and colorspace provide a few more tools to complete the overall look of the app. In addition, extensive CSS is used to create the TRON-inspired look down to details as small as the trophies having a realistic metallic sheen.

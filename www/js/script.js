$(document).on("shiny:sessioninitialized", function(event) {
  var my_cookie = Cookies.get(); 
  Shiny.setInputValue("mydata", my_cookie);
  Shiny.addCustomMessageHandler('user', function(id) {
    Cookies.set('user', id, { expires: 3*365 });
    var update_cookie = Cookies.get();
    Shiny.setInputValue("mydata", update_cookie);
  });
  Shiny.addCustomMessageHandler('logout', function(i) {
    Cookies.remove('user');
    var update_cookie = Cookies.get();
    Shiny.setInputValue("mydata", update_cookie);
  });
  Shiny.setInputValue("loaded", 1);
});